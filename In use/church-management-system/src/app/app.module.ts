import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MemberListComponent } from './member-list/member-list.component';
import { RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    MemberListComponent,
    NotFoundComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule
    , RouterModule.forRoot ([
      {path:'', component: HomeComponent},
      {path:'members', component: MemberListComponent},
      {path:'**', component:NotFoundComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
